const width = 600;
const height = 400;

const modal = d3.select("#modal");
const modalTitle=d3.select(".modal-content").select("h2");
const closeModalBtn = d3.select(".close");
const backButton = d3.select("#backButton");


const slider = d3.select("#slider");
const sliderValue = d3.select("#slider-value").select("span");
const slider2 = d3.select("#slider2");
const slider2Value = document.querySelector('#slider2-value span');

const compareButton= d3.select(".compare-button")


const revealButton = d3.select(".options-button");

let buttonText = "Select countries"; // Initial button text
let newDiv; // Variable to store the created div (for reference)
let countries = []; // Initialize as an array
 let selectedCountries = new Set(countries); 



let isComparisonMode = false;
let countComparison=0;
let compareCountries =[]


const tooltip = d3.select(".tooltip");


const tooltip2 = d3.select(".tooltip2");
let salesData;


function toggleComparisonMode() {
  isComparisonMode = !isComparisonMode;
}

function handleCompareButtonClick() {
  const compareMode = d3.select("#compareMode").property("checked");
  if (compareMode) {
    const selectedCountries = getSelectedCountries(); // Assume this gets the selected countries
    if (selectedCountries.length === 2) {
      selectedCountries.forEach(country => {
        drawGraph(country); // Ensure this appends to the modal content
      });
    } else {
      alert("Please select exactly two countries for comparison.");
    }
  } else {
    // When compare mode is turned off, reset to the single selected country
    const selectedCountry = getSelectedCountry(); // Assume this gets the currently selected country
    showModal(selectedCountry);
  }
}




closeModalBtn.on("click", () => {
  modal.classed("modal-hidden", true);
  setTimeout(() => {
    modal.style("display", "none");
    modal.classed("modal-hidden", false); // Reset for next time
  }, 500); // Match the transition duration
});


backButton.on("click", () => {
  modal.classed("modal-hidden", true);
  setTimeout(() => {
    modal.style("display", "none");
    modal.classed("modal-hidden", false); // Reset for next time
  }, 500); // Match the transition duration
});


function showModal(countryName, countryData, selectedYear) {
  
  modal.style("display", "block");
  modalTitle.text(countryName);
  modalTitle.style("text-align", "center");


  
  drawBarChart(countryName, countryData, selectedYear);
  drawLineChart(countryName, countryData, selectedYear);
  drawStackedBarChart(countryName, countryData);
  
}

function showCompareModal(countryName, countryData, selectedYear, country2Name, country2Data){
  
  modal.style("display", "block");
  
  const chartsContainer = document.getElementById('charts');
  chartsContainer.innerHTML = '';

  
  const row1 = document.createElement('div');
      row1.classList.add('row');

      const barChart1 = document.createElement('div');
      barChart1.id = 'bar-chart';
      barChart1.classList.add('chart');
      row1.appendChild(barChart1);

      const barChart2 = document.createElement('div');
      barChart2.id = 'bar2-chart';
      barChart2.classList.add('chart');
      row1.appendChild(barChart2);

      const row2 = document.createElement('div');
      row2.classList.add('row');

      const lineChart1 = document.createElement('div');
      lineChart1.id = 'line-chart1';
      lineChart1.classList.add('chart');
      row2.appendChild(lineChart1);

      const lineChart2 = document.createElement('div');
      lineChart2.id = 'line-chart2';
      lineChart2.classList.add('chart');
      row2.appendChild(lineChart2);

      chartsContainer.appendChild(row1);
      chartsContainer.appendChild(row2);

      drawBarChart(countryName, countryData, selectedYear, "#bar-chart1");
      drawLineChart(countryName, countryData, selectedYear, "#line-chart1");
    
      // Call drawBarChart and drawLineChart for the second country
      drawBarChart(country2Name, country2Data, selectedYear, "#bar-chart2");
      drawLineChart(country2Name, country2Data, selectedYear, "#line-chart2");

}



const mapContainer = d3
  .select("#map")
  .style("width", "50vw")
  .style("height", "100%");

const svg = mapContainer
  .append("svg")
  .attr("preserveAspectRatio", "xMidYMid meet")
  .attr("viewBox", `0 0 ${width} ${height}`)
  .style("width", "100%")
  .style("height", "100%");



function groupByYearForCountry(countryData, selectedYear) {
  let totalRevenue = 0;
  let totalCost = 0;
  let totalProfit = 0;
  if (countryData) {
    countryData.forEach((item) => {
      const orderYear = new Date(item["Order Date"]).getFullYear();
      if (orderYear === selectedYear) {
        totalRevenue += item["Total Revenue"];
        totalCost += item["Total Cost"];
        totalProfit += item["Total Profit"];
      }
    });
  }
  return {totalRevenue, totalCost,totalProfit};
}



function groupByProductTypeForCountry(countryData, selectedYear) {
  const productTypeMap = {};
  countryData.forEach((item) => {
    const orderYear = new Date(item["Order Date"]).getFullYear();
    if (orderYear === selectedYear) {
      const productType = item["Item Type"];
      if (!productTypeMap[productType]) {
        productTypeMap[productType] = 0;
      }
      productTypeMap[productType] += item["Units Sold"];
    }
  });
  return Object.entries(productTypeMap).map(([product, total]) => ({
    product,
    total,
  }));
}

function drawBarChart(countryName, countryData, selectedYear) {
  const modalContent = d3.select(".modal-content").select("#bar-chart");

  // Remove any existing charts inside the modal content
  modalContent.selectAll(".chart").remove();

  const chartWidth = 600;
  const chartHeight = 400; 
  const margin = { top: 20, right: 90, bottom: 0, left: 60 }; 
  const legend_margin= { top: 20, right: 20, bottom: 60, left: 100 }; 

  // Extract all unique product categories from the data
  const uniqueCategories = [...new Set(countryData.map(d => d["Item Type"]))];

  // Group data by years and categories
  let nestedData = d3.groups(countryData, d => new Date(d["Order Date"]).getFullYear(), d => d["Item Type"])
    .map(([year, categories]) => ({
      year,
      ...Object.fromEntries(categories.map(([category, values]) => [category, d3.sum(values, d => d["Units Sold"])]))
    }));

  // Sort nestedData by year
  nestedData = nestedData.sort((a, b) => a.year - b.year);

  // Get all unique years and sort them
  const years = [...new Set(nestedData.map(d => d.year))].sort((a, b) => a - b);

  // Prepare data for stacking
  const stack = d3.stack()
    .keys(uniqueCategories)
    .value((d, key) => d[key] || 0);

  const series = stack(nestedData);

  const barchartTooltip = d3.select("body").append("div")
    //.attr("class", "tooltip")
    .style("z-index", "100")
    .style("position", "absolute")
    .style("padding", "6px")
    .style("background", "lightgrey")
    .style("border", "1px solid grey")
    .style("border-radius", "3px")
    .style("pointer-events", "none")
    .style("opacity", 0);

  // Append SVG to the modal content
  const svg = modalContent
    .append("svg")
    .attr("class", "chart")
    .attr("preserveAspectRatio", "xMidYMid meet")
    .attr("viewBox", `0 0 ${chartWidth + margin.right} ${chartHeight}`)
    .style("width", "100%")
    .style("height", "100%")
    .append("g")
    .attr("transform", `translate(${margin.left},${margin.top})`);

  const x = d3.scaleBand()
    .domain(years)
    .range([0, chartWidth - margin.left - margin.right])
    .padding(0.1);

  const y = d3.scaleLinear()
    .domain([0, d3.max(series, d => d3.max(d, d => d[1]))])
    .nice()
    .range([chartHeight - margin.top - margin.bottom, 0]);

  const color = d3.scaleOrdinal()
    .range([
      '#e6194B', '#3cb44b', '#ffe119', '#4363d8', '#f58231', '#911eb4', '#42d4f4', '#f032e6', '#bfef45', '#fabed4', '#469990', '#dcbeff', '#9A6324', '#fffac8', '#800000', '#aaffc3', '#808000', '#ffd8b1', '#000075', '#a9a9a9', '#ffffff', '#000000'
    ]);

  // Draw the stacked bars
  svg.selectAll(".serie")
    .data(series)
    .enter().append("g")
    .attr("class", "serie")
    .attr("fill", d => color(d.key))
    .selectAll("rect")
    .data(d => d)
    .enter().append("rect")
    .attr("x", d => x(d.data.year))
    .attr("y", d => y(d[1]))
    .attr("height", d => y(d[0]) - y(d[1]))
    .attr("width", x.bandwidth())
    .on("mouseover", function(event, d) {

      const category = d3.select(this.parentNode).datum().key;
      const heightValue = d[1] - d[0];
      d3.select(this)
        .style("stroke", "black")
        .style("stroke-width", 2)
        .style("opacity", 0.7);
    
        barchartTooltip.transition()
        .duration(200)
        .style("opacity", .9);
        barchartTooltip.html(`<strong>Category:</strong> ${category}<br><strong>Units:</strong> ${heightValue}`)
        .style("left", (event.pageX + 10) + "px")
        .style("top", (event.pageY - 28) + "px");
    })
    .on("mousemove", function(event) {
      barchartTooltip.style("left", (event.pageX + 10) + "px")
        .style("top", (event.pageY - 28) + "px");
    })
    .on("mouseout", function(event, d) {
      d3.select(this)
        .style("stroke", "none")
        .style("opacity", 1);
    
        barchartTooltip.transition()
        .duration(500)
        .style("opacity", 0);
    });
    

  // Add X axis
  svg.append("g")
    .attr("class", "x-axis")
    .attr("transform", `translate(0,${chartHeight - margin.top - margin.bottom})`)
    .call(d3.axisBottom(x).tickFormat(d3.format("d")))
    .selectAll("text")
    .attr("transform", "rotate(-45)")
    .style("text-anchor", "end");

  // Add Y axis
  svg.append("g")
    .attr("class", "y-axis")
    .call(d3.axisLeft(y))
    .append("text")
    .attr("fill", "#000")
    .attr("transform", "rotate(-90)")
    .attr("y", -50)
    .attr("dy", "0em")
    .attr("text-anchor", "end")
    .text("Units Sold");

  svg.append("text")
    .attr("x", (chartWidth - margin.left - margin.right) / 2)
    .attr("y", -margin.top / 2)
    .attr("text-anchor", "middle")
    .style("font-size", "16px")
    .style("font-weight", "bold")
    .text("Sales data for different types of products");

  // Add legend
  const legend = svg.append("g")
    .attr("class", "bar-legend")
    .attr("transform", `translate(${chartWidth - legend_margin.left}, 0)`); // Positioning the legend outside the chart

  uniqueCategories.forEach((category, i) => {
    const legendRow = legend.append("g")
      .attr("transform", `translate(0,${i * 20})`);

    legendRow.append("rect")
      .attr("width", 10)
      .attr("height", 10)
      .attr("fill", color(category));

    legendRow.append("text")
      .attr("x", 20)
      .attr("y", 10)
      .attr("text-anchor", "start")
      .text(category);
  });
}


function drawLineChartForEurope(allCountryData, selectedYear) {
  const europeLineChart = d3.select("#europe-charts");

  europeLineChart.selectAll("svg").remove();
  europeLineChart.selectAll(".chart").remove();

  const chartWidth = 600;
  const chartHeight = 400;
  const margin = { top: 20, right: 40, bottom: 60, left: 60 }; 
  const legend_margin = { top: 20, right: 40, bottom: 60, left: 100 };

  const allRevenues = [];
  const allCosts = [];
  const allProfits = [];
  const years = [];

  // Extract all years from the data
  allCountryData.forEach(countryData => {
      for (let item of countryData) {
          const orderYear = new Date(item["Order Date"]).getFullYear();
          if (!years.includes(orderYear)) {
              years.push(orderYear);
          }
      }
  });

  years.sort((a, b) => a - b); // Sort years

  // Group data by year for all countries
  for (let year of years) {
      let totalRevenue = 0;
      let totalCost = 0;
      let totalProfit = 0;
      allCountryData.forEach(countryData => {
          const { totalRevenue: countryRevenue, totalCost: countryCost, totalProfit: countryProfit } = groupByYearForCountry(countryData, year);
          totalRevenue += countryRevenue;
          totalCost += countryCost;
          totalProfit += countryProfit;
      });
      allRevenues.push(totalRevenue);
      allCosts.push(totalCost);
      allProfits.push(totalProfit);
  }

  // Append a container for the line chart inside the modal content
  const lineChartContainer = europeLineChart.append("div")
      .attr("id", "line-chart")
      .attr("class", "chart")
      .style("width", `${chartWidth}px`)
      .style("height", `${chartHeight}px`);

  // Append SVG to the line chart container
  const svg = lineChartContainer
      .append("svg")
      .attr("preserveAspectRatio", "xMidYMid meet")
      .attr("viewBox", `0 0 ${chartWidth} ${chartHeight}`)
      .style("width", "100%")
      .style("height", "100%")
      .append("g")
      .attr("transform", `translate(${margin.left},${margin.top})`);

  const tooltip2 = d3.select("#europe-charts").select("#line-chart")
      .append("div")
      .attr("class", "tooltip2")
      .style("position", "absolute")
      .style("background", "white")
      .style("padding", "5px")
      .style("border", "1px solid black")
      .style("border-radius", "5px")
      .style("pointer-events", "none")
      .style("opacity", 0); 

  svg.on("mousemove", function(event) {
    const [mouseX, mouseY] = d3.pointer(event); // Get mouse position relative to the container
    const reversedY = chartHeight - (mouseY - margin.top) - 105; // Calculate reversed Y position

    // Show tooltip
    tooltip2.style("opacity", 1)
      .html(`Y position: ${reversedY}`)
      .style("left", `${mouseX}px`) // Position tooltip relative to mouse cursor
      .style("top", `${mouseY}px`);
  });

  svg.on("mouseout", function(event) {
    // Hide tooltip
    tooltip2.style("opacity", 0);
  });

  const xScale = d3.scaleBand()
      .domain(years)
      .range([0, chartWidth - margin.left - margin.right])
      .padding(0.1);

  const yScale = d3.scaleLinear()
      .domain([0, d3.max([d3.max(allRevenues), d3.max(allCosts), d3.max(allProfits)])])
      .nice()
      .range([chartHeight - margin.top - margin.bottom, 0]);

  const line = d3.line()
      .x((d, i) => xScale(years[i]) + xScale.bandwidth() / 2)
      .y(d => yScale(d))
      .curve(d3.curveMonotoneX);

  const yGrid = d3.axisLeft(yScale).tickSize(-chartWidth + 100).tickFormat(""); 

  svg.append("g")
      .style("stroke", "lightgray")  // Set stroke color
      .style("opacity", 0.5)
      .call(yGrid);  

  // Initial empty datasets
  let currentRevenues = [];
  let currentCosts = [];
  let currentProfits = [];

  // Draw initial empty lines
  const revenueLine = svg.append("path")
      .datum(currentRevenues)
      .attr("fill", "none")
      .attr("stroke", "blue")
      .attr("stroke-width", 2)
      .attr("d", line);

  const costLine = svg.append("path")
      .datum(currentCosts)
      .attr("fill", "none")
      .attr("stroke", "green")
      .attr("stroke-width", 2)
      .attr("d", line);

  const profitLine = svg.append("path")
      .datum(currentProfits)
      .attr("fill", "none")
      .attr("stroke", "red")
      .attr("stroke-width", 2)
      .attr("d", line);

  // Animate the lines by updating the data year by year
  years.forEach((year, i) => {
    setTimeout(() => {
      currentRevenues.push(allRevenues[i]);
      currentCosts.push(allCosts[i]);
      currentProfits.push(allProfits[i]);

      revenueLine.datum(currentRevenues)
        .transition()
        .duration(1000)
        .attr("d", line);

      costLine.datum(currentCosts)
        .transition()
        .duration(1000)
        .attr("d", line);

      profitLine.datum(currentProfits)
        .transition()
        .duration(1000)
        .attr("d", line);
    }, i * 500);
  });

  // Add X axis
  svg.append("g")
      .attr("class", "x-axis")
      .attr("transform", `translate(0,${chartHeight - margin.top - margin.bottom})`)
      .call(d3.axisBottom(xScale))
      .selectAll("text")
      .attr("transform", "rotate(-45)")
      .style("text-anchor", "end");

  // Add Y axis
  svg.append("g")
      .attr("class", "y-axis")
      .call(d3.axisLeft(yScale)
          .tickFormat(d3.format(".2s"))) // Format tick values using d3.format
      .append("text")
      .attr("fill", "#000")
      .attr("transform", "rotate(-90)")
      .attr("y", -2)
      .attr("dy", "-4em")
      .attr("text-anchor", "end")
      .text("Values");

  svg.append("text")
      .attr("x", (chartWidth - margin.left - margin.right) / 2)
      .attr("y", -margin.top / 2)
      .attr("text-anchor", "middle")
      .style("font-size", "16px")
      .style("font-weight", "bold")
      .text("Revenue, Cost, and Profit over Years in Europe");

  // Add legend
  const legend = svg.append("g")
      .attr("class", "legend")
      .attr("transform", `translate(${chartWidth - margin.right - 150}, 20)`); // Adjust the translate value

  const legendItems = ["Total Revenue", "Total Cost", "Total Profit"];

  legendItems.forEach((item, i) => {
    const legendItem = legend.append("g")
        .attr("transform", `translate(0, ${i * 20})`);

    legendItem.append("rect")
        .attr("width", 10)
        .attr("height", 10)
        .attr("fill", ["blue", "green", "red"][i]); // Match the colors with the lines

    legendItem.append("text")
        .attr("x", 20)
        .attr("y", 10)
        .attr("text-anchor", "start")
        .text(item);
  });
}


function drawStackedBarChart(countryName, countryData) {
  const years = [];
  const onlineOrders = [];
  const offlineOrders = [];
  
  const modalContent = d3.select(".modal-content").select("#stackedBar-chart");

  modalContent.selectAll(".chart").remove();

  // Initialize years and order counts
  for (let i = 2010; i <= 2017; i++) {
    years.push(i.toString());
    onlineOrders.push(0);
    offlineOrders.push(0);
  }

  // Populate online and offline order counts by year
  countryData.forEach(order => {
    const orderYear = new Date(order["Order Date"]).getFullYear();
    if (years.includes(orderYear.toString())) {
      const index = years.indexOf(orderYear.toString());
      if (order["Sales Channel"] === "Online") {
        onlineOrders[index] += 1;
      } else if (order["Sales Channel"] === "Offline") {
        offlineOrders[index] += 1;
      }
    }
  });

  const data = years.map((year, i) => ({
    year,
    Online: onlineOrders[i],
    Offline: offlineOrders[i]
  }));

  const margin = { top: 50, right: 40, bottom: 60, left: 60 };
  const chartWidth = 600 - margin.left - margin.right;
  const chartHeight = 400 - margin.top - margin.bottom;

  const svg = modalContent
    .append("svg")
    .attr("class", "chart")
    .attr("preserveAspectRatio", "xMidYMid meet")
    //.attr("viewBox", `0 0 ${chartWidth + margin.right} ${chartHeight}`)
    .style("width", "100%")
    .style("height", "60vh")
    .append("g")
    .attr("transform", `translate(${margin.left},${margin.top})`);

  const xScale = d3.scaleBand()
    .domain(years)
    .range([0, chartWidth])
    .padding(0.1);

  const yScale = d3.scaleLinear()
    .domain([0, 8])
    .nice()
    .range([chartHeight, 0]);

  const color = d3.scaleOrdinal()
    .domain(["Online", "Offline"])
    .range(["#1f77b4", "#ff7f0e"]);

  const stack = d3.stack()
    .keys(["Online", "Offline"]);

  const series = stack(data);

  svg.append("g")
    .selectAll("g")
    .data(series)
    .enter().append("g")
    .attr("fill", d => color(d.key))
    .selectAll("rect")
    .data(d => d)
    .enter().append("rect")
    .attr("x", d => xScale(d.data.year))
    .attr("y", d => yScale(d[1]))
    .attr("height", d => yScale(d[0]) - yScale(d[1]))
    .attr("width", xScale.bandwidth());

  svg.append("g")
    .attr("class", "x-axis")
    .attr("transform", `translate(0,${chartHeight})`)
    .call(d3.axisBottom(xScale));

  svg.append("g")
    .attr("class", "y-axis")
    .call(d3.axisLeft(yScale))
    .append("text")
    .attr("fill", "#000")
    .attr("transform", "rotate(-90)")
    .attr("y", -50)
    .attr("dy", "0em")
    .attr("text-anchor", "end")
    .text("Number of channels");

  svg.append("text")
    .attr("x", chartWidth / 2)
    .attr("y", -margin.top / 2)
    .attr("text-anchor", "middle")
    .style("font-size", "16px")
    .style("font-weight", "bold")
    .text("Number of Online and Offline Orders by Year");

  const legend = svg.selectAll(".legend")
    .data(["Online", "Offline"])
    .enter().append("g")
    .attr("class", "legend")
    .attr("transform", (d, i) => `translate(0,${i * 20})`);

  legend.append("rect")
    .attr("x", chartWidth - 18)
    .attr("width", 18)
    .attr("height", 18)
    .style("fill", color);

  legend.append("text")
    .attr("x", chartWidth - 24)
    .attr("y", 9)
    .attr("dy", ".35em")
    .style("text-anchor", "end")
    .text(d => d);
}





function drawLineChart(countryName, countryData, selectedYear) {
  const allRevenues = [];
  const allCosts = [];
  const allProfits = [];
  const years = [];

  // Populate years array
  for (let i = 2010; i <= 2017; i++) {
    years.push(i.toString());
  }

  const countrySalesData = salesData[countryName];

  // Group sales data by year for the selected country
  for (let i = 0; i < years.length; i++) {
    const { totalRevenue, totalCost, totalProfit } = groupByYearForCountry(countrySalesData, parseInt(years[i]));
    allRevenues.push(totalRevenue);
    allCosts.push(totalCost);
    allProfits.push(totalProfit);
  }

  const modalContent = d3.select(".modal-content").select("#line-chart");
  modalContent.selectAll(".chart").remove();

  const chartWidth = 600; // Match bar chart width
  const chartHeight = 400; // Match bar chart height
  const margin = { top: 20, right: 40, bottom: 60, left: 60 };
  const legend_margin = { top: 20, right: 40, bottom: 60, left: 100 };

  const svg = modalContent
    .append("svg")
    .attr("id", "line-chart")
    .attr("class", "chart")
    .attr("preserveAspectRatio", "xMidYMid meet")
    .attr("viewBox", `0 0 ${chartWidth + margin.right} ${chartHeight}`)
    .style("width", "100%")
    .style("height", "60vh")
    .append("g")
    .attr("transform", `translate(${margin.left},${margin.top})`);

  const xScale = d3.scaleBand()
    .domain(years)
    .range([0, chartWidth - margin.left - margin.right])
    .padding(0.1);

  const yScale = d3.scaleLinear()
    .domain([0, d3.max([d3.max(allRevenues), d3.max(allCosts), d3.max(allProfits)])])
    .nice()
    .range([chartHeight - margin.top - margin.bottom, 0]);

  const line = d3.line()
    .x((d, i) => xScale(years[i]) + xScale.bandwidth() / 2)
    .y(d => yScale(d))
    .curve(d3.curveMonotoneX);

  const yGrid = d3.axisLeft(yScale).tickSize(-chartWidth + 100).tickFormat("");

  // Add gridlines before drawing axes
  svg.append("g")
    .style("stroke", "lightgray")
    .style("opacity", 0.5)
    .call(yGrid);

  // Initial empty datasets
  let currentRevenues = [];
  let currentCosts = [];
  let currentProfits = [];

  // Draw initial empty lines
  const revenueLine = svg.append("path")
    .datum(currentRevenues)
    .attr("fill", "none")
    .attr("stroke", "blue")
    .attr("stroke-width", 2)
    .attr("d", line);

  const costLine = svg.append("path")
    .datum(currentCosts)
    .attr("fill", "none")
    .attr("stroke", "green")
    .attr("stroke-width", 2)
    .attr("d", line);

  const profitLine = svg.append("path")
    .datum(currentProfits)
    .attr("fill", "none")
    .attr("stroke", "red")
    .attr("stroke-width", 2)
    .attr("d", line);

  // Animate the lines by updating the data year by year
  years.forEach((year, i) => {
    setTimeout(() => {
      currentRevenues.push(allRevenues[i]);
      currentCosts.push(allCosts[i]);
      currentProfits.push(allProfits[i]);

      revenueLine.datum(currentRevenues)
        .transition()
        .duration(100)
        .attr("d", line);

      costLine.datum(currentCosts)
        .transition()
        .duration(100)
        .attr("d", line);

      profitLine.datum(currentProfits)
        .transition()
        .duration(100)
        .attr("d", line);
    }, i * 1000);
  });

  svg.append("text")
    .attr("x", (chartWidth - margin.left - margin.right) / 2)
    .attr("y", -margin.top / 2)
    .attr("text-anchor", "middle")
    .style("font-size", "16px")
    .style("font-weight", "bold")
    .text("Revenue, Cost, and Profit over Years");

  const legend = svg
    .append("g")
    .attr("class", "legend")
    .attr("transform", `translate(${chartWidth - 220}, 20)`);

  const legendItems = ["Total Revenue", "Total Cost", "Total Profit"];
  const legendWidth = 150;
  const legendHeight = legendItems.length * 20 + 10;

  legend.append("rect")
    .attr("width", legendWidth)
    .attr("height", legendHeight)
    .attr("fill", "white")
    .attr("rx", 4)
    .attr("ry", 4);

  legendItems.forEach((item, i) => {
    const legendItem = legend.append("g")
      .attr("transform", `translate(0, ${i * 20})`);

    legendItem.append("rect")
      .attr("width", 10)
      .attr("height", 10)
      .attr("fill", ["blue", "green", "red"][i]);

    legendItem.append("text")
      .attr("x", 15)
      .attr("y", 9)
      .attr("dy", "0.35em")
      .text(item);
  });

  svg.append("g")
    .attr("class", "x-axis")
    .attr("transform", `translate(0,${chartHeight - margin.top - margin.bottom})`)
    .call(d3.axisBottom(xScale).tickFormat(d3.format("d")))
    .selectAll("text")
    .attr("transform", "rotate(-45)")
    .style("text-anchor", "end");

  svg.append("g")
    .attr("class", "y-axis")
    .call(d3.axisLeft(yScale).tickFormat(d3.format(".2s")))
    .append("text")
    .attr("fill", "#000")
    .attr("transform", "rotate(-90)")
    .attr("y", -50)
    .attr("dy", "0em")
    .attr("text-anchor", "end")
    .text("Values");
}


function animateSlider(){
  const duration = 3000; 
  const startValue=2010;
  const endValue=2017;

  slider.transition()
    .duration(duration)
    .tween("value", function() {
      const interpolate = d3.interpolateNumber(startValue, endValue);
      return function(t) {
          const value = interpolate(t);
          slider.property("value", value); 
          sliderValue.text(Math.round(value));
          updateColorScale(Math.round(value));
      };
  })
}



function filterSalesDataBySelectedCountries(salesData, selectedCountries) {
  const filteredSalesData = {};

  selectedCountries.forEach(country => {
      if (salesData[country]) {
          filteredSalesData[country] = salesData[country];
      }
  });

  return filteredSalesData;
}






let filter;

d3.json("grouped_data.json").then(function (data) {
  salesData = data;
  filter= filterSalesDataBySelectedCountries(salesData, selectedCountries);


  d3.json("europe_geo_filtered.json").then(function (data) {
    const projection = d3.geoMercator().fitSize([width, height], data);
    const path = d3.geoPath().projection(projection);

    // Filter out countries with 0 revenue, profit, and cost for the selected year
    const filteredCountries = Object.entries(salesData).filter(([countryName, countryData]) => {
      const { totalRevenue, totalCost, totalProfit } = groupByYearForCountry(countryData, 2010); // Change 2010 to the selected year
      return (totalRevenue !== 0 || totalCost !== 0 || totalProfit !== 0) && countryData.length > 0;
    });

    // Draw map
    svg
      .selectAll("path")
      .data(data.features.filter(d => filteredCountries.map(([countryName, _]) => countryName).includes(d.properties.NAME))) // Filter the features based on the filtered countries
      .enter()
      .append("path")
      .attr("d", path)
      .attr("fill", function (d) {
        return "lightblue"; // Default color
      })
      .attr("stroke", "white")
      .attr("stroke-width", 0.5)
      .style("cursor", "pointer")
      .on("mouseover", function (event, d) {
        const countryName = d.properties.NAME;
        d3.select(this)
            .attr("original-fill", d3.select(this).attr("fill")) // Store original color
            .attr("fill", "orange"); // Change fill to highlight the country
        tooltip
            .style("opacity", 1)
            .style('cursor', 'pointer')
            .html(countryName)
            .style("left", `${event.clientX + 10}px`)
            .style("top", `${event.clientY - 20}px`);
      })
      .on("mousemove", function (event) {
        tooltip
          .style("left", `${event.clientX + 10}px`)
          .style("top", `${event.clientY - 20}px`);
      })
      .on("mouseout", function (event, d) {
        d3.select(this)
            .attr("fill", d3.select(this).attr("original-fill")); // Revert to original color
        tooltip.style("opacity", 0);
      })
      
      .on("click", function (event, d) {
        const countryName = d.properties.NAME;
        const selectedYear = parseInt(document.getElementById("slider").value);
        const countrySalesData = salesData[countryName];
        const {totalRevenue} = groupByYearForCountry(
          countrySalesData,
          selectedYear
        );
        if (isComparisonMode) {
          compareCountries.push(countryName, countrySalesData, selectedYear);
          if (compareCountries.length >= 6) { // Check if two countries have been selected
              showCompareModal(...compareCountries);
              compareCountries = []; // Reset array for next comparison
              toggleComparisonMode(); // Turn off comparison mode
          }
      } else {
          showModal(countryName, countrySalesData, selectedYear);
      }
  
      //document.getElementById("revenue-display").textContent = `Total Revenue (${selectedYear}) for ${countryName}: $${totalRevenue}`;
      console.log('Clicked:', countryName); 
      });
      //drawLineChartForEurope(Object.values(salesData), 2010);
  });
});



console.log("Filtered Sales Data:", filter);



const labels = ['Total Revenue', 'Total Cost', 'Total Profit'];

slider.on("input", function () {
  const value = +this.value;
  sliderValue.text(value);
  updateColorScale(value);

  const selectedCountry = d3.select("#revenue-display").text().split(" ")[5];
  
});

slider2.on("input", function() {
  const value = parseInt(this.value, 10);
  console.log("Selected value:", value); // Log the selected value to the console
  const options = document.querySelectorAll('#rangeValues option');
  const selectedOption = Array.from(options).find(option => parseInt(option.value, 10) === value);
  const selectedText = selectedOption.textContent; 
  console.log("Selected text:", selectedText); // Log the selected text to the console
  slider2Value.textContent = selectedText; 
  console.log("Selected value:", slider2Value);
  updateColorScale(parseInt(slider.property("value"))); 
});



const initialValue = 2010;
slider.property("value", initialValue);
sliderValue.text(initialValue);
slider2.property("value", 0); 
slider2Value.textContent = 'Total Revenue';


function updateColorScale(year) {
  const allRevenues = [];
  const allCosts = [];
  const allProfits = [];
  let values = [];

  svg.selectAll("path").each(function (d) {
    const countryName = d.properties.NAME;
    const countrySalesData = salesData[countryName];
    const { totalRevenue, totalCost, totalProfit } = groupByYearForCountry(countrySalesData, year);
    allRevenues.push(totalRevenue);
    allCosts.push(totalCost);
    allProfits.push(totalProfit);
  });

  const slider2ValueIndex = parseInt(slider2.property("value"), 10); // Get the value of the slider as an integer
  if (slider2ValueIndex === 0) {
    values = allRevenues;
  } else if (slider2ValueIndex === 1) {
    values = allCosts;
  } else {
    values = allProfits;
  }

  const highestValue = Math.max(...values);
  const lowestValue = Math.min(...values);
  const numSegments = 5;
  

  const colorScale = d3
    .scaleLinear()
    .range(["lightblue", "#001489"])
    .domain([lowestValue, highestValue]);

  // Update legend text
  d3.select("#legend-min").text(lowestValue.toLocaleString());
  d3.select("#legend-max").text(highestValue.toLocaleString());

  // Update legend gradient
  const legendGradient = d3.select(".color-gradient");
  legendGradient.style(
    "background",
    `linear-gradient(to right, ${colorScale(lowestValue)}, ${colorScale(highestValue)})`
  );

  svg.selectAll("path").attr("fill", function (d) {
    const countryName = d.properties.NAME;
    const countrySalesData = salesData[countryName];
    const { totalRevenue, totalCost, totalProfit } = groupByYearForCountry(countrySalesData, year);
    if (slider2ValueIndex === 0) {
      return colorScale(totalRevenue); 
    } else if (slider2ValueIndex === 1) {
      return colorScale(totalCost); 
    } else {
      return colorScale(totalProfit); 
    }
  });
}

const sliderAnimation = d3.select(".animation-button");
sliderAnimation.on("click", animateSlider);



compareButton.on('click', handleCompareButtonClick)


updateColorScale(initialValue); // Initialize color scale

/////////////////////////////////////////////////////////

const filteredSalesData = filterSalesDataBySelectedCountries(salesData, selectedCountries);

function drawMapAndSalesData() {
  d3.json("europe_geo_filtered.json").then(function (geoData) {
      const width = 960;
      const height = 800;
      const projection = d3.geoMercator().fitSize([width, height], geoData);
      const path = d3.geoPath().projection(projection);

      // Filter sales data based on selected countries
      const filteredSalesData = filterSalesDataBySelectedCountries(salesData, selectedCountries);

      
            drawLineChartForEurope(Object.values(filteredSalesData), 2010);
  });
}



//////////////////////////////////////////////

d3.json("grouped_data.json").then(function (data) {
  // Assuming the data is an object where keys are country names
  countries = Object.keys(data); // Extract the country names as an array
  salesData=data
  console.log("Extracted country names:", countries);

  drawMapAndSalesData();
});

//////////////////////////////////////////////

function createNewDiv() {
  // Select container (assuming it exists elsewhere)
  const container = d3.select("#selection-row");

  // Check if a div already exists
  if (!newDiv) {
      newDiv = container.append("div");

      // Add buttons for checking/unchecking all
      newDiv.append("button")
          .text("Check All")
          .style("padding", "5px")
          .style("margin-right", "10px")
          .on("click", checkAllCheckboxes)
          ;

      newDiv.append("button")
          .text("Uncheck All")
          .style("padding", "5px")
          .on("click", uncheckAllCheckboxes);

      // Create an unordered list to hold countries
      const countryList = newDiv.append("ul").attr("class", "country-list");

      // Loop through countries and add them as list items with checkboxes
      countries.forEach(country => {
          const listItem = countryList.append("li");
          const label = listItem.append("label").text(country);
          label.append("input")
              .attr("type", "checkbox")
              .attr("value", country)
              .property("checked", true) // Default state is checked
              .on("change", handleCheckboxChange)
              .style("margin-left", "5px");
      });

      // Customize the new div (optional)
      newDiv.style("background-color", "lightblue");
      newDiv.style("padding", "10px");
  } else {
      // Toggle visibility of the existing div
      const isHidden = newDiv.style("display") === "none";
      newDiv.style("display", isHidden ? "block" : "none");
  }

  // Toggle button text
  buttonText = buttonText === "Select countries" ? "Hide selection" : "Select countries";
  revealButton.text(buttonText);
}

function handleCheckboxChange() {
  const checkbox = d3.select(this);
  const country = checkbox.property("value");
  
  if (checkbox.property("checked")) {
      selectedCountries.add(country);
  } else {
      selectedCountries.delete(country);
  }

  updateSelectedList();
  console.log("Selected countries:", Array.from(selectedCountries));

  drawMapAndSalesData();
}

function updateSelectedList() {
  const selectedList = d3.select("#selected-list");
  selectedList.html(""); // Clear the current list

  selectedCountries.forEach(country => {
      selectedList.append("li").text(country);
  });
}

function checkAllCheckboxes() {
  d3.selectAll(".country-list input[type='checkbox']")
      .property("checked", true)
      .each(function() {
          selectedCountries.add(this.value);
      });

  updateSelectedList();
  drawMapAndSalesData();
}

function uncheckAllCheckboxes() {
  d3.selectAll(".country-list input[type='checkbox']")
      .property("checked", false)
      .each(function() {
          selectedCountries.delete(this.value);
      });

  updateSelectedList();
  drawMapAndSalesData();
}

revealButton.on('click', createNewDiv);

// Ensure the selected list and graph are updated on page load
updateSelectedList();
drawMapAndSalesData(); // Call this to draw the graph initially with all countries selected